package io.kzolotov.navigationbugreproducer

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.compose.setContent
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            Navigation()
        }
    }
}

@Composable
fun Navigation() {
    val navController = rememberNavController()

    val launcher =
        rememberLauncherForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            navController.popBackStack()
        }

    NavHost(navController = navController, startDestination = "start") {
        composable("start") {
            StartScreen(navController)
        }
        composable("another") {
        Log.i("Navigation", "Open another screen")
            AnotherScreen()
        }
        composable("system_settings") {
            Log.i("Navigation", "Open system settings")
            val intent = Intent(Settings.ACTION_SETTINGS)
            launcher.launch(intent)
        }
    }
}

@Composable()
fun StartScreen(navController: NavHostController) {
    Column {
        Button(
            modifier = Modifier.padding(8.dp),
            onClick = { navController.navigate("system_settings") }
        ) {
            Text("Open settings")
        }

        Button(
            modifier = Modifier.padding(8.dp),
            onClick = { navController.navigate("another") }
        ) {
            Text("Open compose screen")
        }
    }
}

@Composable()
fun AnotherScreen() {
    Text("Another screen")
}